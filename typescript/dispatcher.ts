import { Subscription } from "./subscription"
import { ContinueCallback, ResolvedEvent } from "./common"

export type EventHandlerCallback = (data: any, metadata: any, next: ContinueCallback) => void

export class EventHandler {
	public constructor(readonly eventType: string, readonly callback: EventHandlerCallback) { }
}

export class EventHandlerCollection {
	private handlers: EventHandler[] = []
	
	public register(eventType: string, method: EventHandlerCallback): void {
		this.handlers.push(new EventHandler(eventType, method))
	}
	
	public registeredHandlers(): EventHandler[] {
		return this.handlers
	}
}

export class Dispatcher {
	private handlers: Handlers = { }
	private callbacksForCurrentEvent: EventHandlerCallback[]
	private currentIndex: number
	private currentEvent: ResolvedEvent
	private onDone: ContinueCallback
	
	public constructor(private subscription: Subscription) { }
	
	public start(onStarted: () => void): void {
		this.subscription.start((event, next) => {
			if (!this.handlers[event.type]) { return next() } // no handlers for this event
			
			// prepare to dispatch event to registered callbacks
			this.currentIndex = 0
			this.currentEvent = event
			this.onDone = next
			this.callbacksForCurrentEvent = this.handlers[event.type]
			
			this.dispatchEventToCurrentHandler()
		})
		
		onStarted()
	}
	
	public register(collection: EventHandlerCollection): void {
		collection.registeredHandlers().forEach(handler => {
			if (!this.handlers[handler.eventType]) {
				this.handlers[handler.eventType] = []
			}

			this.handlers[handler.eventType].push(handler.callback)
		})
	}
	
	private dispatchEventToCurrentHandler(): void {
		// check there is a next handler, if not return control to subscription
		if (!this.callbacksForCurrentEvent[this.currentIndex]) { return this.onDone() }
		
		// dispatch event to next callback and send a continue method to hand control back to us
		this.callbacksForCurrentEvent[this.currentIndex](this.currentEvent.data, this.currentEvent.metadata, () => {
			this.step()
		})
	}
	
	private step(): void {
		// use set immediate to make this async and close the current call stack
		setImmediate(() => {
			this.currentIndex++
			this.dispatchEventToCurrentHandler()
		})
	}
}

// dictionary type used by the dispatcher
interface Handlers {
	[index: string]: EventHandlerCallback[]
}