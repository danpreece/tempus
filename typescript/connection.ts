import * as request from "request"
import * as keepalive from "agentkeepalive"
import { NewEvent, ResolvedEvent, StreamPage } from "./common"

export interface Options {
	readonly host: string
	readonly port: string
}

export interface Credentials {
	readonly username: string
	readonly password: string
}

export enum ExpectedVersion {
	NewStream = -1,
	Any = -2,
	ShouldExist = -4
}

export interface WriteResult {
	readonly isError: boolean
	readonly message: string
}

export class Connection {
	private readonly agent = new keepalive({
		maxSockets: 5,
		maxFreeSockets: 1,
		timeout: 60000,
		freeSocketKeepAliveTimeout: 30000
	})
	
	private readonly requestWithDefaults = request.defaults({
		agent: this.agent as any
	})
	
	public constructor(private readonly options: Options, private readonly credentials: Credentials) { }
	
	public writeEvents(stream: string, events: NewEvent[], expectedVersion: number = ExpectedVersion.NewStream): Promise<WriteResult> {
		return new Promise((resolve, reject) => {
			try {
				let requestOptions: request.CoreOptions = {
					headers: {
						"Content-Type": "application/vnd.eventstore.events+json",
						"ES-ExpectedVersion": expectedVersion
					},
					json: true,
					body: events
				}
				if (this.credentials) {
					requestOptions.auth = {
						user: this.credentials.username,
						pass: this.credentials.password
					}
				}

				request.post(this.buildStreamUri(stream), requestOptions,
				(error, response, body) => {
					if (error) { return reject(error) }

					switch (response.statusCode) {
						case 201:
							return resolve({isError: false, message: "Events appended to stream successfully"})
						default:
							return resolve({isError: true, message: body})
					}
				})
			} catch (error) {
				reject(error)
			}
		})
	}
	
	public remove(stream: string, hardDelete: boolean = false): Promise<WriteResult> {
		throw new Error("method not implemented")
	}
	
	public headOfStream(stream: string): Promise<StreamPage> {
		return this.pageOfStream(this.buildStreamUri(stream))
	}
	
	public rootOfStream(stream: string): Promise<StreamPage> {
		return this.headOfStream(stream).then(page => {
			return page.last(this)
		})
	}
	
	public pageOfStream(uri: string, eTag?: string, previous?: StreamPage): Promise<StreamPage> {
		return new Promise((resolve, reject) => {
			let options = this.getRequestOptions(eTag)

			// console.log("requesting " + uri + " with " + options)

			this.requestWithDefaults.get(uri + "?embed=tryharder", options, (error, response, body) => {
				if (error) { return reject(error) }

				if (response.statusCode == 304) {
					// content unmodified, this means that we were preforming a long poll and nothing has happened
					return resolve(previous)
				}

				if (response.statusCode != 200) {
					return reject("eventstore responded with " + response.statusCode + " to the request " + uri);
				}

				let streamPage: StreamPage | undefined = this.buildPage(uri, response, body)
				if (!streamPage) { 
					console.error(error, response.request, response.statusCode, response.headers, response.body)
					return reject("eventstore responded with a malformed atom feed to the request " + uri) 
				}

				resolve(streamPage)
			})
		})
	}
	
	private buildStreamUri(stream: string): string {
		return this.options.host + ":" + this.options.port + "/streams/" + stream
	}
	
	private getRequestOptions(eTag?: string): any {
		let options: request.CoreOptions = {
			headers: {
				"Accept": "application/vnd.eventstore.atom+json",
				"Connection": "keep-alive"
			},
			forever: true
		}
		if (this.credentials) {
			options.auth = {
				user: this.credentials.username,
				pass: this.credentials.password
			}
		}
		if (eTag) {
			// check for invalid state
			// will always be defined because it is defined above, but typescript compiler moans without this
			if (!options.headers) { options.headers = {} }
			// preform a long poll
			options.headers[ "ES-LongPoll" ] = 20
			options.headers[ "If-None-Match" ] = eTag
		}

		return options
	}
	
	private buildPage(uri: string, response: request.RequestResponse, body: any): StreamPage | undefined {
		try {
			let data: AtomPage = JSON.parse(body)
			// build events
			let events = data.entries.map((entry: AtomEntry) => {
				try {
					return new ResolvedEvent(
						entry.title,
						entry.id,
						entry.streamId,
						entry.eventType,
						entry.eventNumber,
						JSON.parse(entry.data),
						JSON.parse(entry.metaData || "{}")
					)
				} catch (error) {
					// error will have come from the json decode, so just provide no data or metadata
					return new ResolvedEvent(
						entry.title,
						entry.id,
						entry.streamId,
						entry.eventType,
						entry.eventNumber,
						{},
						{}
					)
				}
			})

			// build links
			let firstLink: AtomLink | undefined = this.extractLink("first", data.links)
			let lastLink: AtomLink | undefined =  this.extractLink("last", data.links)
			let nextLink: AtomLink | undefined =  this.extractLink("next", data.links)
			let previousLink: AtomLink | undefined =  this.extractLink("previous", data.links)

			if ( !(firstLink) ) {
				return undefined
			}
			let lastLinkUri: string | undefined
			if (lastLink) {
				lastLinkUri = lastLink.uri
			}
			let nextLinkUri: string | undefined
			if (nextLink) {
				nextLinkUri = nextLink.uri
			}
			let previousLinkUri: string | undefined
			if (previousLink) {
				previousLinkUri = previousLink.uri
			}

			return new StreamPage(
				data.title,
				data.id,
				data.headOfStream,
				events,
				response.headers.etag as string,
				uri, // self
				firstLink.uri,
				lastLinkUri,
				nextLinkUri,
				previousLinkUri
			)

		} catch (error) {
			console.error("tempus building stream page", error)
			return undefined
		}
	}
	
	private extractLink = (key: string, links: AtomLink[]): AtomLink | undefined => {
		return links.find((value: AtomLink) => {
			if (value.relation == key) { return true }
			return false
		})
	}
}

interface AtomLink {
	uri: string
	relation: string
}

interface AtomEntry {
	title: string
	id: string
	updated: string
	author: {
		name: string
	}
	summary: string
	eventType: string
	eventNumber: number
	isJson: boolean,
	data: string
	streamId: string
	metaData: string | undefined
	links: AtomLink[]
}

interface AtomPage {
	title: string
	id: string
	updated: string
	author: {
		name: string
	}
	headOfStream: boolean
	links: AtomLink[],
	entries: AtomEntry[]
}
