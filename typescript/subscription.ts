import * as fs from "fs"
import { Connection } from "./connection"
import { ContinueCallback, ResolvedEvent, StreamPage } from "./common"

export type OnEventCallback = (event: ResolvedEvent, next: ContinueCallback) => void

interface CatchupSubscriptionCache {
	page: any
	index: number
}

export interface Subscription {
	start(onEvent: OnEventCallback): Promise<any>
}

export class CatchupSubscription implements Subscription {
	private onEvent: OnEventCallback
	private currentPage: StreamPage
	private currentIndex: number
	
	public constructor(
		private connection: Connection,
		private stream: string,
		private cacheLocation: string) { }
		
	public start(onEvent: OnEventCallback): Promise<any> {
		return new Promise(async (resolve, reject) => {
			try {
				let cache = await this.loadCache()
				// cache found
				this.setNewPage(cache.page)
			} catch (error) {
				// no cache found
				this.setNewPage(await this.connection.rootOfStream(this.stream))
			}
			
			this.onEvent = onEvent
			this.currentIndex = 0
			this.nextEvent()
		})
	}
	
	private nextEvent(): void {
		if (this.currentPage.events[this.currentIndex]) {
			// there is an event to process, send it to the client code along with a continue function to hand control back to us
			this.onEvent(this.currentPage.events[this.currentIndex], () => { this.step() })
		} else {
			// no events to process, get the next page
			this.nextPage()
		}
	}
	
	private nextPage(): void {
		this.currentPage.previousOrPoll(this.connection).then(page => {
			this.setNewPage(page)
			this.currentIndex = 0
			this.writeCache()
			this.nextEvent()
		}).catch(error => {
			console.error("Tempus CatchupSubscription", "error loading next page:", error)
		})
	}
	
	// control is passed back to us from the client code, cache to progress and continue
	private step(): void {
		// use set immediate to make this asynchronus and let the current call stack close
		setImmediate(() => {
			this.currentIndex++
			this.writeCache()
			this.nextEvent()
		})
	}
	
	private setNewPage(page: StreamPage): void {
		this.currentPage = page
		this.currentPage.events.reverse()
	}
	
	private loadCache(): Promise<CatchupSubscriptionCache> {
		return new Promise((resolve, reject) => {
			try {
				let path = this.buildCachePath(this.cacheLocation, this.stream)
				if (!fs.existsSync(path)) { return reject("no cache found") }

				let buffer = fs.readFileSync(path)
				let cache = JSON.parse(buffer.toString()) as CatchupSubscriptionCache
				resolve({
					page: new StreamPage(
						cache.page.title,
						cache.page.id,
						cache.page.isHead,
						cache.page.events,
						cache.page.eTag,
						cache.page.selfLink,
						cache.page.firstLink,
						cache.page.lastLink,
						cache.page.nextLink,
						cache.page.previousLink
					),
					index: cache.index
				})
			} catch (error) {
				reject(error)
			}
		})
	}
	
	private writeCache(): void {
		if (!fs.existsSync(this.cacheLocation)) {
			fs.mkdirSync(this.cacheLocation)
		}
		let path = this.buildCachePath(this.cacheLocation, this.stream)
		fs.writeFileSync(path, JSON.stringify({
			page: this.currentPage,
			index: this.currentIndex
		}))
	}
	
	private buildCachePath(cacheLocation: string, stream: string): string {
		return cacheLocation + stream + ".cache"
	}
}