import { Connection } from "./connection"

export type ContinueCallback = () => void

export class NewEvent {
	public constructor(
		readonly eventId: string, // unique identifier for a new event
		readonly eventType: string, // a label for the type of event
		readonly data: any, // the json data contained within the event
		readonly metadata?: any) { } // the json metadata contained within the event
}

export class ResolvedEvent {
	public constructor(
		readonly title: string,
		readonly id: string,
		readonly streamId: string,
		readonly type: string,
		readonly number: number,
		readonly data: object,
		readonly metadata?: object) { }
}

export class StreamPage {
	public constructor(
		readonly title: string,
		readonly id: string,
		readonly isHead: boolean,
		readonly events: ResolvedEvent[],
		readonly eTag: string,
		readonly selfLink: string,
		readonly firstLink: string,
		readonly lastLink: string | undefined,
		readonly nextLink: string | undefined,
		readonly previousLink: string | undefined) { }
		
	public first(connection: Connection): Promise<StreamPage> {
		return connection.pageOfStream(this.firstLink)
	}
	
	public last(connection: Connection): Promise<StreamPage> {
		if (!this.lastLink) {
			return Promise.resolve(this)
		}
		
		return connection.pageOfStream(this.lastLink)
	}
	
	public hasNext(): boolean {
		return !(this.nextLink == undefined)
	}
	
	public next(connection: Connection): Promise<StreamPage> {
		if (!this.nextLink) {
			return Promise.reject("no next link available")
		}
		
		return connection.pageOfStream(this.nextLink)
	}
	
	public hasPrevious(): boolean {
		return !(this.previousLink == undefined)
	}
	
	public previous(connection: Connection): Promise<StreamPage> {
		if (!this.previousLink) {
			return Promise.reject("no previous link available")
		}
		
		return connection.pageOfStream(this.previousLink)
	}
	
	public poll(connection: Connection): Promise<StreamPage> {
		if (this.previousLink) {
			return Promise.reject("a stream page with a previous link should not be polled")
		}
		
		return connection.pageOfStream(this.selfLink,  this.eTag, this)
	}
	
	public previousOrPoll(connection: Connection): Promise<StreamPage> {
		if (this.previousLink) { return this.previous(connection) }
		
		return this.poll(connection)
	}
	
	public toJSON(): any {
		return {
			title: this.title,
			id: this.id,
			isHead: this.isHead,
			events: this.events,
			eTag: this.eTag,
			selfLink: this.selfLink,
			firstLink: this.firstLink,
			lastLink: this.lastLink,
			nextLink: this.nextLink,
			previousLink: this.previousLink
		}
	}
}