import { NewEvent, StreamPage } from "./common";
export interface Options {
    readonly host: string;
    readonly port: string;
}
export interface Credentials {
    readonly username: string;
    readonly password: string;
}
export declare enum ExpectedVersion {
    NewStream = -1,
    Any = -2,
    ShouldExist = -4,
}
export interface WriteResult {
    readonly isError: boolean;
    readonly message: string;
}
export declare class Connection {
    private readonly options;
    private readonly credentials;
    private readonly agent;
    private readonly requestWithDefaults;
    constructor(options: Options, credentials: Credentials);
    writeEvents(stream: string, events: NewEvent[], expectedVersion?: number): Promise<WriteResult>;
    remove(stream: string, hardDelete?: boolean): Promise<WriteResult>;
    headOfStream(stream: string): Promise<StreamPage>;
    rootOfStream(stream: string): Promise<StreamPage>;
    pageOfStream(uri: string, eTag?: string, previous?: StreamPage): Promise<StreamPage>;
    private buildStreamUri(stream);
    private getRequestOptions(eTag?);
    private buildPage(uri, response, body);
    private extractLink;
}
