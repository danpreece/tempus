"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const common_1 = require("./common");
class CatchupSubscription {
    constructor(connection, stream, cacheLocation) {
        this.connection = connection;
        this.stream = stream;
        this.cacheLocation = cacheLocation;
    }
    start(onEvent) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            try {
                let cache = yield this.loadCache();
                // cache found
                this.setNewPage(cache.page);
            }
            catch (error) {
                // no cache found
                this.setNewPage(yield this.connection.rootOfStream(this.stream));
            }
            this.onEvent = onEvent;
            this.currentIndex = 0;
            this.nextEvent();
        }));
    }
    nextEvent() {
        if (this.currentPage.events[this.currentIndex]) {
            // there is an event to process, send it to the client code along with a continue function to hand control back to us
            this.onEvent(this.currentPage.events[this.currentIndex], () => { this.step(); });
        }
        else {
            // no events to process, get the next page
            this.nextPage();
        }
    }
    nextPage() {
        this.currentPage.previousOrPoll(this.connection).then(page => {
            this.setNewPage(page);
            this.currentIndex = 0;
            this.writeCache();
            this.nextEvent();
        }).catch(error => {
            console.error("Tempus CatchupSubscription", "error loading next page:", error);
        });
    }
    // control is passed back to us from the client code, cache to progress and continue
    step() {
        // use set immediate to make this asynchronus and let the current call stack close
        setImmediate(() => {
            this.currentIndex++;
            this.writeCache();
            this.nextEvent();
        });
    }
    setNewPage(page) {
        this.currentPage = page;
        this.currentPage.events.reverse();
    }
    loadCache() {
        return new Promise((resolve, reject) => {
            try {
                let path = this.buildCachePath(this.cacheLocation, this.stream);
                if (!fs.existsSync(path)) {
                    return reject("no cache found");
                }
                let buffer = fs.readFileSync(path);
                let cache = JSON.parse(buffer.toString());
                resolve({
                    page: new common_1.StreamPage(cache.page.title, cache.page.id, cache.page.isHead, cache.page.events, cache.page.eTag, cache.page.selfLink, cache.page.firstLink, cache.page.lastLink, cache.page.nextLink, cache.page.previousLink),
                    index: cache.index
                });
            }
            catch (error) {
                reject(error);
            }
        });
    }
    writeCache() {
        if (!fs.existsSync(this.cacheLocation)) {
            fs.mkdirSync(this.cacheLocation);
        }
        let path = this.buildCachePath(this.cacheLocation, this.stream);
        fs.writeFileSync(path, JSON.stringify({
            page: this.currentPage,
            index: this.currentIndex
        }));
    }
    buildCachePath(cacheLocation, stream) {
        return cacheLocation + stream + ".cache";
    }
}
exports.CatchupSubscription = CatchupSubscription;
