"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class EventHandler {
    constructor(eventType, callback) {
        this.eventType = eventType;
        this.callback = callback;
    }
}
exports.EventHandler = EventHandler;
class EventHandlerCollection {
    constructor() {
        this.handlers = [];
    }
    register(eventType, method) {
        this.handlers.push(new EventHandler(eventType, method));
    }
    registeredHandlers() {
        return this.handlers;
    }
}
exports.EventHandlerCollection = EventHandlerCollection;
class Dispatcher {
    constructor(subscription) {
        this.subscription = subscription;
        this.handlers = {};
    }
    start(onStarted) {
        this.subscription.start((event, next) => {
            if (!this.handlers[event.type]) {
                return next();
            } // no handlers for this event
            // prepare to dispatch event to registered callbacks
            this.currentIndex = 0;
            this.currentEvent = event;
            this.onDone = next;
            this.callbacksForCurrentEvent = this.handlers[event.type];
            this.dispatchEventToCurrentHandler();
        });
        onStarted();
    }
    register(collection) {
        collection.registeredHandlers().forEach(handler => {
            if (!this.handlers[handler.eventType]) {
                this.handlers[handler.eventType] = [];
            }
            this.handlers[handler.eventType].push(handler.callback);
        });
    }
    dispatchEventToCurrentHandler() {
        // check there is a next handler, if not return control to subscription
        if (!this.callbacksForCurrentEvent[this.currentIndex]) {
            return this.onDone();
        }
        // dispatch event to next callback and send a continue method to hand control back to us
        this.callbacksForCurrentEvent[this.currentIndex](this.currentEvent.data, this.currentEvent.metadata, () => {
            this.step();
        });
    }
    step() {
        // use set immediate to make this async and close the current call stack
        setImmediate(() => {
            this.currentIndex++;
            this.dispatchEventToCurrentHandler();
        });
    }
}
exports.Dispatcher = Dispatcher;
