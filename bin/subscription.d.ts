import { Connection } from "./connection";
import { ContinueCallback, ResolvedEvent } from "./common";
export declare type OnEventCallback = (event: ResolvedEvent, next: ContinueCallback) => void;
export interface Subscription {
    start(onEvent: OnEventCallback): Promise<any>;
}
export declare class CatchupSubscription implements Subscription {
    private connection;
    private stream;
    private cacheLocation;
    private onEvent;
    private currentPage;
    private currentIndex;
    constructor(connection: Connection, stream: string, cacheLocation: string);
    start(onEvent: OnEventCallback): Promise<any>;
    private nextEvent();
    private nextPage();
    private step();
    private setNewPage(page);
    private loadCache();
    private writeCache();
    private buildCachePath(cacheLocation, stream);
}
