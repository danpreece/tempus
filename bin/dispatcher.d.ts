import { Subscription } from "./subscription";
import { ContinueCallback } from "./common";
export declare type EventHandlerCallback = (data: any, metadata: any, next: ContinueCallback) => void;
export declare class EventHandler {
    readonly eventType: string;
    readonly callback: EventHandlerCallback;
    constructor(eventType: string, callback: EventHandlerCallback);
}
export declare class EventHandlerCollection {
    private handlers;
    register(eventType: string, method: EventHandlerCallback): void;
    registeredHandlers(): EventHandler[];
}
export declare class Dispatcher {
    private subscription;
    private handlers;
    private callbacksForCurrentEvent;
    private currentIndex;
    private currentEvent;
    private onDone;
    constructor(subscription: Subscription);
    start(onStarted: () => void): void;
    register(collection: EventHandlerCollection): void;
    private dispatchEventToCurrentHandler();
    private step();
}
