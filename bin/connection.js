"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const request = require("request");
const keepalive = require("agentkeepalive");
const common_1 = require("./common");
var ExpectedVersion;
(function (ExpectedVersion) {
    ExpectedVersion[ExpectedVersion["NewStream"] = -1] = "NewStream";
    ExpectedVersion[ExpectedVersion["Any"] = -2] = "Any";
    ExpectedVersion[ExpectedVersion["ShouldExist"] = -4] = "ShouldExist";
})(ExpectedVersion = exports.ExpectedVersion || (exports.ExpectedVersion = {}));
class Connection {
    constructor(options, credentials) {
        this.options = options;
        this.credentials = credentials;
        this.agent = new keepalive({
            maxSockets: 5,
            maxFreeSockets: 1,
            timeout: 60000,
            freeSocketKeepAliveTimeout: 30000
        });
        this.requestWithDefaults = request.defaults({
            agent: this.agent
        });
        this.extractLink = (key, links) => {
            return links.find((value) => {
                if (value.relation == key) {
                    return true;
                }
                return false;
            });
        };
    }
    writeEvents(stream, events, expectedVersion = ExpectedVersion.NewStream) {
        return new Promise((resolve, reject) => {
            try {
                let requestOptions = {
                    headers: {
                        "Content-Type": "application/vnd.eventstore.events+json",
                        "ES-ExpectedVersion": expectedVersion
                    },
                    json: true,
                    body: events
                };
                if (this.credentials) {
                    requestOptions.auth = {
                        user: this.credentials.username,
                        pass: this.credentials.password
                    };
                }
                request.post(this.buildStreamUri(stream), requestOptions, (error, response, body) => {
                    if (error) {
                        return reject(error);
                    }
                    switch (response.statusCode) {
                        case 201:
                            return resolve({ isError: false, message: "Events appended to stream successfully" });
                        default:
                            return resolve({ isError: true, message: body });
                    }
                });
            }
            catch (error) {
                reject(error);
            }
        });
    }
    remove(stream, hardDelete = false) {
        throw new Error("method not implemented");
    }
    headOfStream(stream) {
        return this.pageOfStream(this.buildStreamUri(stream));
    }
    rootOfStream(stream) {
        return this.headOfStream(stream).then(page => {
            return page.last(this);
        });
    }
    pageOfStream(uri, eTag, previous) {
        return new Promise((resolve, reject) => {
            let options = this.getRequestOptions(eTag);
            // console.log("requesting " + uri + " with " + options)
            this.requestWithDefaults.get(uri + "?embed=tryharder", options, (error, response, body) => {
                if (error) {
                    return reject(error);
                }
                if (response.statusCode == 304) {
                    // content unmodified, this means that we were preforming a long poll and nothing has happened
                    return resolve(previous);
                }
                if (response.statusCode != 200) {
                    return reject("eventstore responded with " + response.statusCode + " to the request " + uri);
                }
                let streamPage = this.buildPage(uri, response, body);
                if (!streamPage) {
                    console.error(error, response.request, response.statusCode, response.headers, response.body);
                    return reject("eventstore responded with a malformed atom feed to the request " + uri);
                }
                resolve(streamPage);
            });
        });
    }
    buildStreamUri(stream) {
        return this.options.host + ":" + this.options.port + "/streams/" + stream;
    }
    getRequestOptions(eTag) {
        let options = {
            headers: {
                "Accept": "application/vnd.eventstore.atom+json",
                "Connection": "keep-alive"
            },
            forever: true
        };
        if (this.credentials) {
            options.auth = {
                user: this.credentials.username,
                pass: this.credentials.password
            };
        }
        if (eTag) {
            // check for invalid state
            // will always be defined because it is defined above, but typescript compiler moans without this
            if (!options.headers) {
                options.headers = {};
            }
            // preform a long poll
            options.headers["ES-LongPoll"] = 20;
            options.headers["If-None-Match"] = eTag;
        }
        return options;
    }
    buildPage(uri, response, body) {
        try {
            let data = JSON.parse(body);
            // build events
            let events = data.entries.map((entry) => {
                try {
                    return new common_1.ResolvedEvent(entry.title, entry.id, entry.streamId, entry.eventType, entry.eventNumber, JSON.parse(entry.data), JSON.parse(entry.metaData || "{}"));
                }
                catch (error) {
                    // error will have come from the json decode, so just provide no data or metadata
                    return new common_1.ResolvedEvent(entry.title, entry.id, entry.streamId, entry.eventType, entry.eventNumber, {}, {});
                }
            });
            // build links
            let firstLink = this.extractLink("first", data.links);
            let lastLink = this.extractLink("last", data.links);
            let nextLink = this.extractLink("next", data.links);
            let previousLink = this.extractLink("previous", data.links);
            if (!(firstLink)) {
                return undefined;
            }
            let lastLinkUri;
            if (lastLink) {
                lastLinkUri = lastLink.uri;
            }
            let nextLinkUri;
            if (nextLink) {
                nextLinkUri = nextLink.uri;
            }
            let previousLinkUri;
            if (previousLink) {
                previousLinkUri = previousLink.uri;
            }
            return new common_1.StreamPage(data.title, data.id, data.headOfStream, events, response.headers.etag, uri, // self
            firstLink.uri, lastLinkUri, nextLinkUri, previousLinkUri);
        }
        catch (error) {
            console.error("tempus building stream page", error);
            return undefined;
        }
    }
}
exports.Connection = Connection;
