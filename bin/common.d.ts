import { Connection } from "./connection";
export declare type ContinueCallback = () => void;
export declare class NewEvent {
    readonly eventId: string;
    readonly eventType: string;
    readonly data: any;
    readonly metadata: any;
    constructor(eventId: string, eventType: string, data: any, metadata?: any);
}
export declare class ResolvedEvent {
    readonly title: string;
    readonly id: string;
    readonly streamId: string;
    readonly type: string;
    readonly number: number;
    readonly data: object;
    readonly metadata: object | undefined;
    constructor(title: string, id: string, streamId: string, type: string, number: number, data: object, metadata?: object | undefined);
}
export declare class StreamPage {
    readonly title: string;
    readonly id: string;
    readonly isHead: boolean;
    readonly events: ResolvedEvent[];
    readonly eTag: string;
    readonly selfLink: string;
    readonly firstLink: string;
    readonly lastLink: string | undefined;
    readonly nextLink: string | undefined;
    readonly previousLink: string | undefined;
    constructor(title: string, id: string, isHead: boolean, events: ResolvedEvent[], eTag: string, selfLink: string, firstLink: string, lastLink: string | undefined, nextLink: string | undefined, previousLink: string | undefined);
    first(connection: Connection): Promise<StreamPage>;
    last(connection: Connection): Promise<StreamPage>;
    hasNext(): boolean;
    next(connection: Connection): Promise<StreamPage>;
    hasPrevious(): boolean;
    previous(connection: Connection): Promise<StreamPage>;
    poll(connection: Connection): Promise<StreamPage>;
    previousOrPoll(connection: Connection): Promise<StreamPage>;
    toJSON(): any;
}
