"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class NewEvent {
    constructor(eventId, // unique identifier for a new event
        eventType, // a label for the type of event
        data, // the json data contained within the event
        metadata) {
        this.eventId = eventId;
        this.eventType = eventType;
        this.data = data;
        this.metadata = metadata;
    } // the json metadata contained within the event
}
exports.NewEvent = NewEvent;
class ResolvedEvent {
    constructor(title, id, streamId, type, number, data, metadata) {
        this.title = title;
        this.id = id;
        this.streamId = streamId;
        this.type = type;
        this.number = number;
        this.data = data;
        this.metadata = metadata;
    }
}
exports.ResolvedEvent = ResolvedEvent;
class StreamPage {
    constructor(title, id, isHead, events, eTag, selfLink, firstLink, lastLink, nextLink, previousLink) {
        this.title = title;
        this.id = id;
        this.isHead = isHead;
        this.events = events;
        this.eTag = eTag;
        this.selfLink = selfLink;
        this.firstLink = firstLink;
        this.lastLink = lastLink;
        this.nextLink = nextLink;
        this.previousLink = previousLink;
    }
    first(connection) {
        return connection.pageOfStream(this.firstLink);
    }
    last(connection) {
        if (!this.lastLink) {
            return Promise.resolve(this);
        }
        return connection.pageOfStream(this.lastLink);
    }
    hasNext() {
        return !(this.nextLink == undefined);
    }
    next(connection) {
        if (!this.nextLink) {
            return Promise.reject("no next link available");
        }
        return connection.pageOfStream(this.nextLink);
    }
    hasPrevious() {
        return !(this.previousLink == undefined);
    }
    previous(connection) {
        if (!this.previousLink) {
            return Promise.reject("no previous link available");
        }
        return connection.pageOfStream(this.previousLink);
    }
    poll(connection) {
        if (this.previousLink) {
            return Promise.reject("a stream page with a previous link should not be polled");
        }
        return connection.pageOfStream(this.selfLink, this.eTag, this);
    }
    previousOrPoll(connection) {
        if (this.previousLink) {
            return this.previous(connection);
        }
        return this.poll(connection);
    }
    toJSON() {
        return {
            title: this.title,
            id: this.id,
            isHead: this.isHead,
            events: this.events,
            eTag: this.eTag,
            selfLink: this.selfLink,
            firstLink: this.firstLink,
            lastLink: this.lastLink,
            nextLink: this.nextLink,
            previousLink: this.previousLink
        };
    }
}
exports.StreamPage = StreamPage;
