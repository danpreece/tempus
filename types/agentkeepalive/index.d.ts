declare class Agent {
	constructor(options: Agent.Options)
}

declare namespace Agent {
	export interface Options {
		keepAlive?: boolean
		freeSocketKeepAliveTimeout?: number
		keepAliveTimeout?: number
		timeout?: number
		keepAliveMsecs?: number
		maxSockets?: number
		maxFreeSockets?: number
		socketActiveTTL?: number
	}
}

export = Agent